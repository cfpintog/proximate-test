export interface Menu{
    id: number,
    icon: string,
    productId: string,
    redirectTo: string
}