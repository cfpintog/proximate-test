export interface Product{
    id: number,
    image: string,
    longDescription: string,
    path: string,
    shortDescription: string,
    title: string
}